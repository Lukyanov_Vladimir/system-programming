package ru.lva.copyFiles;

public class CopyStream extends Thread {
    private String inputFileLink, outputFileLink;

    CopyStream(String inputFileLink, String outputFileLink) {
        this.inputFileLink = inputFileLink;
        this.outputFileLink = outputFileLink;
    }

    @Override
    public void run() {
        CopyFile cf = new CopyFile(inputFileLink, outputFileLink);
        cf.copy();
    }
}
