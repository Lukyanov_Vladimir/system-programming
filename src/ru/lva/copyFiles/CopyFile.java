package ru.lva.copyFiles;

import java.io.*;

class CopyFile {
    private String inputFileLink, outputFileLink;

    CopyFile(String inputFileLink, String outputFileLink) {
        this.inputFileLink = inputFileLink;
        this.outputFileLink = outputFileLink;
    }

    void copy() {
        try (BufferedReader br = new BufferedReader(new FileReader(inputFileLink))) {
            FileWriter fw = new FileWriter(outputFileLink);

            String str;
            while ((str = br.readLine()) != null) {
                fw.write(str + "\n");
            }

            fw.close();

        } catch (FileNotFoundException e) {
            System.err.println("Файл не найден!");
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
