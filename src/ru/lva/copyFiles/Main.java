package ru.lva.copyFiles;

public class Main {
    private static final String LINK_FILE_ONE = "src\\ru\\lva\\copyFiles\\fileOne.txt";
    private static final String LINK_FILE_ONE_COPY = "src\\ru\\lva\\copyFiles\\fileOne_(copy).txt";
    private static final String LINK_FILE_TWO = "src\\ru\\lva\\copyFiles\\fileTwo.txt";
    private static final String LINK_FILE_TWO_COPY = "src\\ru\\lva\\copyFiles\\fileTwo_(copy).txt";

    public static void main(String[] args) {
        long timeBefore = System.currentTimeMillis();
        CopyStream cs = new CopyStream(LINK_FILE_ONE, LINK_FILE_ONE_COPY);
        CopyStream cs2 = new CopyStream(LINK_FILE_TWO, LINK_FILE_TWO_COPY);
        cs.start();
        cs2.start();
        try {
            cs.join();
            cs2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long timeAfter = System.currentTimeMillis();

        System.out.println("Время параленльного копирования: " + time(timeBefore, timeAfter) + " ms");

        timeBefore = System.currentTimeMillis();
        CopyFile cf = new CopyFile(LINK_FILE_ONE, LINK_FILE_ONE_COPY);
        cf.copy();
        CopyFile cf2 = new CopyFile(LINK_FILE_TWO, LINK_FILE_TWO_COPY);
        cf2.copy();
        timeAfter = System.currentTimeMillis();

        System.out.println("Время последовательного копирования: " + time(timeBefore, timeAfter) + " ms");
    }

    private static long time(long before, long after) {
        return after - before;
    }
}
