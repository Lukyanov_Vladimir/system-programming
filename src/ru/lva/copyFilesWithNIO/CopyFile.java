package ru.lva.copyFilesWithNIO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

class CopyFile {
    private Path inputFileLink, outputFileLink;

    CopyFile(String inputFileLink, String outputFileLink) {
        this.inputFileLink = Paths.get(inputFileLink);
        this.outputFileLink = Paths.get(outputFileLink);
    }

    void copy() {
        try {
            Files.copy(inputFileLink, outputFileLink, StandardCopyOption.REPLACE_EXISTING);
        } catch (FileNotFoundException e) {
            System.err.println("Файл не найден!");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
