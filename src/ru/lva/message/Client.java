package ru.lva.message;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
             BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8))) {
            Scanner sc = new Scanner(System.in);
            String message = sc.nextLine();
            out.write(message + "\r\n");
            System.out.println("Серверу отправлено: " + message);
            out.flush();
            System.out.println("Сервер прислал: " + in.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}