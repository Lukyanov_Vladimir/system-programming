package ru.lva.message;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        try (Socket clientSocket = serverSocket.accept();
             BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), Charset.forName("UTF-8")));
             BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), Charset.forName("UTF-8")))) {

            System.out.println("Новое подключение " + clientSocket.getInetAddress().toString());

            String message = in.readLine();

            out.write(message + "\r\n");
            System.out.println("Прислал клиент: " + message);
            out.flush();

            System.out.println("Клиент отсоединился " + clientSocket.getInetAddress().toString());
        }
    }
}