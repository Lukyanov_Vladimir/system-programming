package ru.lva.bankAccount.main;

import ru.lva.bankAccount.account.Account;
import ru.lva.bankAccount.thread.AccountThreadAddMoney;
import ru.lva.bankAccount.thread.AccountThreadTakeMoney;
import ru.lva.bankAccount.thread.ThreadCheckMoney;

public class Main {
    public static void main(String[] args) {
        try {
            Account account = new Account(2000);
            AccountThreadAddMoney accountThreadAddMoney = new AccountThreadAddMoney(account);
            AccountThreadTakeMoney accountThreadTakeMoney = new AccountThreadTakeMoney(account, 50_000);
            ThreadCheckMoney threadCheckMoney = new ThreadCheckMoney(account, 20_000);

            accountThreadAddMoney.start();
            accountThreadTakeMoney.start();
            threadCheckMoney.start();

            accountThreadAddMoney.join();

            System.out.println("Остаток: " + account.getBalance());
        } catch (Exception e) {
            System.err.println("Что-то пошло не так...");
            e.printStackTrace();
        }
    }
}
