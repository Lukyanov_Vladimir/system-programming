package ru.lva.bankAccount.account;

import ru.lva.bankAccount.exeption.BalanceExeption;
import ru.lva.bankAccount.exeption.NegativeExeption;

public class Account {
    private long balance;

    public Account() {
        this(0);
    }

    public Account(long balance) {
        this.balance = balance;
    }

    public long getBalance() {
        return balance;
    }

    public synchronized void addMoney(long money) throws NegativeExeption {
        checkNegativeNum(money);
        balance += money;
        notify();
    }

    public void takeMoney(long money) throws BalanceExeption, NegativeExeption {
        checkNegativeNum(money);
        checkBalance(money);
        balance -= money;
    }

    private void checkNegativeNum(long money) throws NegativeExeption {
        if (money < 0) {
            throw new NegativeExeption("Введённое вами число меньше нуля");
        }
    }

    private void checkBalance(long money) throws BalanceExeption {
        if (balance < money) {
            throw new BalanceExeption("Недостатосно средств для снятия");
        }
    }

    public synchronized boolean checkLimitMoney(long money) throws InterruptedException {
        while (balance <= money) {
            wait();
        }
        notify();
        return true;
    }
}
