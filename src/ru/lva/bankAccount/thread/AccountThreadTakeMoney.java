package ru.lva.bankAccount.thread;

import ru.lva.bankAccount.account.Account;
import ru.lva.bankAccount.exeption.BalanceExeption;
import ru.lva.bankAccount.exeption.NegativeExeption;

public class AccountThreadTakeMoney extends Thread {
    private final Account account;
    private long money;

    public AccountThreadTakeMoney(Account account, long money) {
        this.account = account;
        this.money = money;
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            try {
                account.takeMoney(100);
            } catch (NegativeExeption | BalanceExeption e) {
                e.printStackTrace();
            }
        }
    }
}

