package ru.lva.bankAccount.thread;

import ru.lva.bankAccount.account.Account;
import ru.lva.bankAccount.exeption.NegativeExeption;

public class AccountThreadAddMoney extends Thread {
    private final Account account;

    public AccountThreadAddMoney(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 20; i++) {
                account.addMoney(1500);
            }
        } catch (NegativeExeption e) {
            e.printStackTrace();
        }
    }
}
