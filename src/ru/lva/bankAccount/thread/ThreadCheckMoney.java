package ru.lva.bankAccount.thread;

import ru.lva.bankAccount.account.Account;
import ru.lva.bankAccount.exeption.BalanceExeption;
import ru.lva.bankAccount.exeption.NegativeExeption;

public class ThreadCheckMoney extends Thread{
    private final Account account;
    private long money;

    public ThreadCheckMoney(Account account, long money) {
        this.account = account;
        this.money = money;
    }

    @Override
    public void run() {
        try {
            if (account.checkLimitMoney(money)) {
                account.takeMoney(money);
            }
        } catch (InterruptedException | BalanceExeption | NegativeExeption e) {
            e.printStackTrace();
        }
    }
}
