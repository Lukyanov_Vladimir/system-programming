package ru.lva.multiplication;

/*
  @author Лукьянов В. А. 17ит18
 */

import java.util.Scanner;

/**
 * Класс математических операций
 */
public class Multiplication {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(multiply(sc.nextInt(), sc.nextInt()));
    }

    /**
     * Функция вычисления произведения
     *
     * @param multiplicand - множимое (первый параметр)
     * @param multiplier   - множитель (второй параметр)
     * @return - возврат результата  произведения
     */

    static int multiply(int multiplicand, int multiplier) {
        int temp, result = 0;

        if (multiplicand > multiplier) {
            temp = multiplicand;
            multiplicand = multiplier;
            multiplier = temp;
        }

        for (int i = 0; i < Math.abs(multiplier); i++) {
            if (multiplicand < 0 && multiplier < 0) {
                result -= multiplicand;
            } else {
                result += multiplicand;
            }
        }

        return result;
    }
}