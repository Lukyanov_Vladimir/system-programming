package ru.lva.statisticalData.statistic;

public class Statistic {

    public static int[] process(String str) {
        int caharacters = countCharacters(str);

        return new int[]{wordCount(str), caharacters, (caharacters - countSpace(str))};
    }

    private static int countCharacters(String str) {
        return str.length();
    }

    private static int wordCount(String str) {
        int numWords = 0;

        String[] arrWords = str.split(" ");

        for (String arrWord : arrWords) {
            if (!arrWord.equals("") && !arrWord.matches("[^\\w]+")) {
                numWords++;
            }
        }
        return numWords;
    }

    private static int countSpace(String str) {
        int numSpace = 0;

        String[] arrWords = str.split("");

        for (String arrWord : arrWords) {
            if (arrWord.matches("\\s")) {
                numSpace++;
            }
        }
        return numSpace;
    }
}
