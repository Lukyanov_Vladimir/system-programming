package ru.lva.statisticalData.GUI;

import ru.lva.statisticalData.main.Reading;
import ru.lva.statisticalData.main.Record;
import ru.lva.statisticalData.statistic.Statistic;

import javax.swing.*;

import java.awt.*;

public class GUI extends JFrame {
    private int[] values;

    private JPanel panel3 = new JPanel();
    private JTextField textField = new JTextField();
    private JButton record = new JButton("Записать в файл");
    public static JLabel label2 = new JLabel("No data");
    private JLabel label3 = new JLabel("Файл записан в src\\ru\\lva\\statisticalData\\files\\statistics.txt");


    public GUI() {
        super("Статистика");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension sSize = Toolkit.getDefaultToolkit ().getScreenSize();
        int width = 450;
        int height = 300;
        setSize(width, height);
        setLocation((sSize.width - width) / 2,(sSize.height - height) / 2);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JPanel panel1 = new JPanel();
        panel.add(panel1);
        JPanel panel2 = new JPanel();
        panel.add(panel2);
        panel.add(panel3);

        JLabel label = new JLabel("Введите URL:");
        panel1.add(label);
        panel1.add(textField);
        JButton button = new JButton("Готово");
        panel1.add(button);
        panel2.add(label2);

        textField.setColumns(20);

        button.setSize(300, 150);
        button.addActionListener(e -> {
            values = Statistic.process(Reading.readFile(textField.getText()));
            label2.setText("<html>Слов: " + values[0] + "<br/>" +
                    "Символов (с пробелами): " + values[1] + "<br/>" +
                    "Символов (без пробелов): " + values[2] + "</html>");

            panel3.add(record);

            record.setVisible(true);
            label3.setVisible(false);
            record.addActionListener(e1 -> {
                Record.writeFile(values);
                record.setVisible(false);
                panel3.add(label3);
                label3.setVisible(true);
            });
        });

        setContentPane(panel);
    }
}
