package ru.lva.statisticalData.main;

import java.io.FileWriter;
import java.io.IOException;

public class Record {
    public static void writeFile(int[] values) {
        try {
            FileWriter fileWriter = new FileWriter("src\\ru\\lva\\statisticalData\\files\\statistics.txt");

            fileWriter.write("Слов: " + values[0] + "\n" + "Символов (с пробелами): " + values[1] + "\n" + "Символов (без пробелов): " + values[2]);

            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
