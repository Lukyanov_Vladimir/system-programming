package ru.lva.statisticalData.main;

import ru.lva.statisticalData.GUI.GUI;


public class Main {

    public static void main(String[] args) {
        GUI gui = new GUI();
        gui.setVisible(true);
        gui.setResizable(false);
    }
}
