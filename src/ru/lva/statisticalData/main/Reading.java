package ru.lva.statisticalData.main;

import ru.lva.statisticalData.GUI.GUI;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Reading {

    public static String readFile(String url) {
        StringBuilder str = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(url));

            String temp;
            while ((temp = br.readLine()) != null) {
                if (!temp.equals("")) {
                    str.append(temp).append(" ");
                }
            }

            br.close();
        } catch (FileNotFoundException e) {
            System.err.println("Ошибка: Файл не найден!");
            GUI.label2.setText("Ошибка: Файл не найден!");
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return str.substring(0, str.length() - 1);
    }
}
