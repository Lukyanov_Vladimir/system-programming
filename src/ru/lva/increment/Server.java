package ru.lva.increment;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException{
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Ожидание");

        try(Socket clientSocket = serverSocket.accept();
            InputStream inputStream = clientSocket.getInputStream();
            OutputStream outputStream = clientSocket.getOutputStream()){

            System.out.println("Новое соединение:" + clientSocket.getInetAddress().toString());
            int number;
            while ((number = inputStream.read()) != -1){
                outputStream.write(++number);
                outputStream.flush();
            }
            System.out.println("Клиент отключился");
        }
    }
}

