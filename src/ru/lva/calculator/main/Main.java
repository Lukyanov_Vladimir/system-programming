package ru.lva.calculator.main;

import ru.lva.calculator.mathInteger.MathInteger;
import ru.lva.calculator.parse.Parse;
import ru.lva.calculator.readConsole.ReadConsole;
import ru.lva.calculator.split.Split;

public class Main {
    public static void main(String[] args) {

        try {
            int result = choice(Split.split(ReadConsole.input()));
            System.out.println("Ответ: " + result);

        } catch (ArithmeticException e) {
            System.out.println("Ошибка: Неверный ввод данных");
            e.printStackTrace();

        } catch (NumberFormatException e) {
            System.out.println("Ошибка: Неверный формат введенных данных");
            e.printStackTrace();

        } catch (Exception e) {
            System.out.println("Ошибка: Упс... Что-то пошло не так");
            e.printStackTrace();
        }
    }

    /**
     * Метод выполняет математические операции
     *
     * @param array - строковый массив
     * @return - ответ
     */
    private static int choice(String[] array) {
        int valueOne = Parse.parseInt(array[0]);
        int valueTwo = Parse.parseInt(array[2]);

        int answer = 0;

        switch (array[1]) {
            case "*":
                answer = MathInteger.multiply(valueOne, valueTwo);
                break;
            case "/":
                answer = MathInteger.division(valueOne, valueTwo);
                break;
            case "+":
                answer = MathInteger.addition(valueOne, valueTwo);
                break;
            case "-":
                answer = MathInteger.subtraction(valueOne, valueTwo);
                break;
            case "^":
                answer = MathInteger.pow(valueOne, valueTwo);
                break;
            case "%":
                answer = MathInteger.residue(valueOne, valueTwo);
                break;
        }

        return answer;
    }
}
