package ru.lva.calculator.split;

public class Split {
    /**
     * Метод разделяет строку на слова
     *
     * @param expression - срока
     * @return - массив строк
     */
    public static String[] split(String expression) {

        String[] arrayOfCharacters = expression.split(" ");

        return arrayOfCharacters;
    }
}
