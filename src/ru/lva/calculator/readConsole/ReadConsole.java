package ru.lva.calculator.readConsole;

import java.util.Scanner;

public class ReadConsole {
    /**
     * Метод считывает с консоли строку
     *
     * @return - строка
     */
    public static String input() {
        Scanner sc = new Scanner(System.in);
        String expression = sc.nextLine();

        dataCheck(expression);

        return expression;
    }

    /**
     * Метод проверят входящие данные
     *
     * @param expression - строковое выражение
     */
    private static void dataCheck(String expression) {
        if (expression.matches("[+-]?[0-9]+\\s[+-/*^%]?\\s[+-]?[0-9]+")) {
        } else {
            throw new ArithmeticException("Incorrect data entry");
        }
    }
}
