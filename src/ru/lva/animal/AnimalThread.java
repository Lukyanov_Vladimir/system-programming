package ru.lva.animal;

public class AnimalThread extends Thread {
    private String name;
    private int priorityThread;

    AnimalThread(String name, int priorityThread) {
        this.name = name;
        this.priorityThread = priorityThread;
    }

    @Override
    public void run() {
        setName(name);
        setPriority(priorityThread);

        int speed = 1000 / priorityThread;

        int DISTANCE = 100;
        for (int i = 0; i < DISTANCE; i++) {
            try {
                Thread.sleep(speed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(getName()+": "+i);

            if (i == DISTANCE / 2) {
                if (getPriority() == 1) {
                    setPriority(10);
                } else {
                    setPriority(1);
                }

                speed = 1000 / getPriority();
            }
        }

        System.out.println("FINISH");
    }
}
