package ru.lva.calculator2.main;

import ru.lva.calculator2.calc.Calc;
import ru.lva.calculator2.fileReader.ReadFile;
import ru.lva.calculator2.fileWriter.WriteFile;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        try {
            System.out.println("Выберете откуда считывать данные?" +
                    "\n1. Консоль" +
                    "\n2. Файл");
            selection(sc.nextInt());

        } catch (NumberFormatException e) {
            System.out.println("Ошибка: Неверный формат введенных данных");
            e.printStackTrace();

        } catch (ArithmeticException e) {
            System.out.println("Ошибка: Неверный ввод данных");
            e.printStackTrace();

        } catch (Exception e) {
            System.out.println("Ошибка: Упс... Что-то пошло не так");
            e.printStackTrace();
        }
    }

    /**
     * Метод выбора обработки данных
     *
     * @param value - значение
     */
    private static void selection(int value) {
        Scanner sc = new Scanner(System.in);

        switch (value) {
            case 1:
                WriteFile.outputConsole(Calc.calc(ReadFile.inputConsole()));
                break;

            case 2:
                System.out.println("Введите путь к файлу");
                WriteFile.outputFile(Calc.calc(ReadFile.inputFile(sc.nextLine())));
                break;
        }
    }


}
