package ru.lva.calculator2.mathInteger;

/**
 * Класс MathInteger создержит методы: умножение, деление, сложение, вычитание, возведение в степень, выделение остатка
 *
 * @author Лукьянов В. А. 17ИТ18
 */
public class MathInteger {
    /**
     * Метод выполняет умножение двух чисел
     *
     * @param numberOne - первое число
     * @param numberTwo - второе число
     * @return - произведение
     */
    public static int multiply(int numberOne, int numberTwo) {
        long result = (long) numberOne * (long) numberTwo;

        if ((int) result != result) {
            throw new ArithmeticException("integer overflow");
        }

        return (int) result;
    }

    /**
     * Метод выполняет деление двух чисел
     *
     * @param numberOne  - первое число
     * @param numberTwo- второе число
     * @return - частное
     */
    public static int division(int numberOne, int numberTwo) {
        if (numberTwo == 0) {
            throw new ArithmeticException("You can't divide by zero!");
        }

        int result = numberOne / numberTwo;

        if ((numberOne ^ numberTwo) < 0 && (result * numberTwo != numberOne)) {
            result--;
        }

        return result;
    }

    /**
     * Метод выполняет сложение двух чисел
     *
     * @param numberOne  - первое число
     * @param numberTwo- второе число
     * @return - сумма
     */
    public static int addition(int numberOne, int numberTwo) {
        int result = numberOne + numberTwo;

        if (((numberOne ^ result) & (numberTwo ^ result)) < 0) {
            throw new ArithmeticException("integer overflow");
        }

        return result;
    }

    /**
     * Метод выполняет вычитание двух чисел
     *
     * @param numberOne  - первое число
     * @param numberTwo- второе число
     * @return - разность
     */
    public static int subtraction(int numberOne, int numberTwo) {
        long result = (long) numberOne - (long) numberTwo;

        if ((int) result != result) {
            throw new ArithmeticException("integer overflow");
        }

        return (int) result;
    }

    /**
     * Метод возводит в степень
     *
     * @param numberOne  - первое число
     * @param numberTwo- второе число
     * @return - число возведённое в степень
     */
    public static int pow(int numberOne, int numberTwo) {
        if (numberOne < numberTwo) {
            int temp = numberOne;
            numberOne = numberTwo;
            numberTwo = temp;
        }

        long result = numberOne;

        for (int i = 0; i < numberTwo - 1; i++) {
            result *= numberOne;
        }

        if ((int) result != result) {
            throw new ArithmeticException("integer overflow");
        }
        if (numberTwo < 0) {
            throw new ArithmeticException("Negative power forbidden!");
        }

        return (int) result;
    }

    /**
     * Метод выделяет остаток
     *
     * @param valueOne - первое число
     * @param valueTwo - второе число
     * @return - остаток
     */
    public static int residue(int valueOne, int valueTwo) {
        if (valueTwo == 0) {
            throw new ArithmeticException("You can't divide by zero!");
        }

        return valueOne % valueTwo;
    }
}
