package ru.lva.calculator2.calc;

import ru.lva.calculator2.mathInteger.MathInteger;
import ru.lva.calculator2.parse.Parse;

import java.util.ArrayList;

public class Calc {
    /**
     * Метод разделяет строку на слова
     *
     * @param expressions - строка
     * @return - Списочный массив строк
     */
    public static ArrayList<String> calc(ArrayList<String> expressions) {

        for (int i = 0; i < expressions.size(); i++) {
            if (expressions.get(i) != "") {
                String[] arrayOfCharacters = expressions.get(i).split(" ");

                expressions.set(i, solve(arrayOfCharacters[1], Parse.parseInt(arrayOfCharacters[0]), Parse.parseInt(arrayOfCharacters[2])));
            }
        }

        return expressions;
    }

    /**
     * Метод разделяет строку на слова
     *
     * @param expression - строка
     * @return - строку
     */

    public static String calc(String expression) {

        String[] arrayOfCharacters = expression.split(" ");

        expression = solve(arrayOfCharacters[1], Parse.parseInt(arrayOfCharacters[0]), Parse.parseInt(arrayOfCharacters[2]));

        return expression;
    }

    /**
     * Метод вызывает математические операции
     *
     * @param str - строка
     * @param valueOne - первое значение
     * @param valueTwo - второе значение
     */
    private static String solve(String str, int valueOne, int valueTwo) {
        Integer answer = null;

        switch (str) {
            case "*":
                answer = MathInteger.multiply(valueOne, valueTwo);
                break;
            case "/":
                answer = MathInteger.division(valueOne, valueTwo);
                break;
            case "+":
                answer = MathInteger.addition(valueOne, valueTwo);
                break;
            case "-":
                answer = MathInteger.subtraction(valueOne, valueTwo);
                break;
            case "^":
                answer = MathInteger.pow(valueOne, valueTwo);
                break;
            case "%":
                answer = MathInteger.residue(valueOne, valueTwo);
                break;
        }

        return answer.toString();
    }
}
