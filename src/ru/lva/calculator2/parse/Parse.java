package ru.lva.calculator2.parse;

public class Parse {

    /**
     * Метод преобразует строку в целочисленное значение
     *
     * @param str - строка
     * @return - преобразованное значение
     */
    public static int parseInt(String str) {
        return Integer.parseInt(str);
    }
}
