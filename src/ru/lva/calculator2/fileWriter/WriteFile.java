package ru.lva.calculator2.fileWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WriteFile {
    /**
     * Метод выыводит строку в консоль
     *
     * @param answer - строка
     */
    public static void outputConsole(String answer) {
        System.out.println("Ответ: " + answer);
    }

    /**
     * Метод записывает строковый массив в файл
     *
     * @param answer - строковый массив
     */
    public static void outputFile(ArrayList<String> answer) {
        try {
            FileWriter fileWriter = new FileWriter("src\\ru\\lva\\calculator2\\files\\textOutput.txt");

            for (int i = 0; i < answer.size(); i++) {
                if (answer.get(i) != "") {
                    fileWriter.write("Ответ: " + answer.get(i) + "\n");

                } else {
                    fileWriter.write("\n");
                }
            }

            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
