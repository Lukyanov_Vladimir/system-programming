package ru.lva.sortByFrequency;

import java.util.Arrays;

public class SortByFrequency {
    public static void main(String[] args) {
        int[] arr = {2, 5, 2, 8, 5, 6, 8, 8};
        System.out.println("Ответ: " + Arrays.toString(sort(arr)));
    }

    /**
     * Метод производит сортировку исходный массив по частоте
     *
     * @param arr - массив
     * @return - возврат отсортированного массива
     */
    static int[] sort(int[] arr) {

        int[][] twoDimensionalArray = sortByFrequency(arr);

        int counter = 0;

        for (int i = 0; i < twoDimensionalArray[1].length; i++) {
            int numberOccurrences = 0, value = 0, index = 0;

            for (int j = 0; j < twoDimensionalArray[1].length; j++) {
                if (numberOccurrences < twoDimensionalArray[1][j] && twoDimensionalArray[1][j] != 0) {
                    numberOccurrences = twoDimensionalArray[1][j];
                    index = j;
                    value = twoDimensionalArray[0][j];
                }
            }
            for (int g = 0; g < numberOccurrences; g++) {
                arr[counter] = value;
                counter++;
            }
            twoDimensionalArray[1][index] = 0;
        }
        return arr;
    }

    /**
     * Метод создаёт двумерный массив, записывая число с кол-вом вхождений
     *
     * @param arr - массив
     * @return - возврат отсортированного двумероного массива с числомами и кол-вом их вхождений
     */
    private static int[][] sortByFrequency(int[] arr) {
        int[][] twoDimensionalArray = new int[2][arr.length];

        int counter = 0;

        for (int value : arr) {
            if (checkForMatchingItems(twoDimensionalArray, value)) {
                twoDimensionalArray[0][counter] = value;
                for (int i : arr) {
                    if (twoDimensionalArray[0][counter] == i) {
                        twoDimensionalArray[1][counter]++;
                    }
                }
                counter++;
            }
        }
        return twoDimensionalArray;
    }

    /**
     * Метод проверяет содержит ли двумерный массив похожее число и возвращает true или false
     *
     * @param arr    - массив
     * @param number - число
     * @return - возврат результата проверки на повтор чисел
     */

    private static boolean checkForMatchingItems(int[][] arr, int number) {
        boolean permission = true;

        for (int j = 0; j < arr[0].length; j++) {
            if (arr[0][j] == number && arr[1][j] != 0) {
                permission = false;
                break;
            }
        }
        return permission;
    }
}
