package ru.lva.tenThread;

public class Main extends Thread {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            (new Main()).start();
        }
        System.out.println("Глвный поток запущен");

    }

    @Override
    public void run() {
        System.out.println("Hello from a thread! " + getName());
    }
}
