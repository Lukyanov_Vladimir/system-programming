package ru.lva.multiplication;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplicationTest {

    @Test
    void randomNumbers() {
        int num1 = (int) (Math.random());
        int num2 = (int) (Math.random());
        assertEquals(num1 * num2, Multiplication.multiply(num1, num2));
    }

    @Test
    void positiveNumbers() {
        int num1 = 1 + (int) (Math.random() * 200);
        int num2 = 1 + (int) (Math.random() * 100);
        assertEquals(num1 * num2, Multiplication.multiply(num1, num2));
    }

    @Test
    void zeroByZero() {
        int num1 = 0;
        int num2 = 0;
        assertEquals(num1 * num2, Multiplication.multiply(num1, num2));
    }

    @Test
    void negativeNumbers() {
        int num1 = -10 + (int) (Math.random() * 10);
        int num2 = -10 + (int) (Math.random() * 10);
        assertEquals(num1 * num2, Multiplication.multiply(num1, num2));
    }
}