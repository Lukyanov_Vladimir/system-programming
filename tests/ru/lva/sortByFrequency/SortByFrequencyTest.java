package ru.lva.sortByFrequency;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortByFrequencyTest {

    @Test
    void oneTestArray() {
        int[] inPut = {2, 5, 2, 8, 5, 6, 8, 8};
        int[] outPut = {8, 8, 8, 2, 2, 5, 5, 6};
        assertArrayEquals(outPut, SortByFrequency.sort(inPut));
    }

    @Test
    void twoTestArray() {
        int[] inPut = {2, 5, 2, 6, -1, 9999999, 5, 8, 8, 8};
        int[] outPut = {8, 8, 8, 2, 2, 5, 5, 6, -1, 9999999};
        assertArrayEquals(outPut, SortByFrequency.sort(inPut));
    }

    @Test
    void testArrayWithZero() {
        int[] inPut = {2, 5, 0, 2, 0, 6, -1, 9999999, 5, 8, 8, 0, 0, 8};
        int[] outPut = {0, 0, 0, 0, 8, 8, 8, 2, 2, 5, 5, 6, -1, 9999999};
        assertArrayEquals(outPut, SortByFrequency.sort(inPut));
    }
}