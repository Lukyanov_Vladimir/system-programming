/*
package ru.lva.calculator.mathInteger;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MathIntegerTest {

    @Test
    void additionPositive() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.addition(numberOne, numberTwo);
        assertEquals(numberOne + numberTwo, result);
    }

    @Test
    void additionNegative() {
        int numberOne = -1 + (int) (Math.random() * -100);
        int numberTwo = -1 + (int) (Math.random() * -100);
        int result = MathInteger.addition(numberOne, numberTwo);
        assertEquals(numberOne + numberTwo, result);
    }

    @Test
    void additionZero() {
        int numberOne = 0;
        int numberTwo = 0;
        int result = MathInteger.addition(numberOne, numberTwo);
        assertEquals(numberOne + numberTwo, result);
    }

    @Test
    void additionFirstNumberZero() {
        int numberOne = 0;
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.addition(numberOne, numberTwo);
        assertEquals(numberOne + numberTwo, result);
    }

    @Test
    void additionSecondNumberZero() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 0;
        int result = MathInteger.addition(numberOne, numberTwo);
        assertEquals(numberOne + numberTwo, result);
    }

    @Test()
    void additionOverflow() {
        int numberOne = 2147483647;
        int numberTwo = 5;
        int result = MathInteger.addition(numberOne, numberTwo);
        assertEquals(numberOne + numberTwo, result);
    }


    @Test
    void subtractionPositive() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.subtraction(numberOne, numberTwo);
        assertEquals(numberOne - numberTwo, result);
    }

    @Test
    void subtractionNegative() {
        int numberOne = -1 + (int) (Math.random() * -100);
        int numberTwo = -1 + (int) (Math.random() * -100);
        int result = MathInteger.subtraction(numberOne, numberTwo);
        assertEquals(numberOne - numberTwo, result);
    }

    @Test
    void subtractionZero() {
        int numberOne = 0;
        int numberTwo = 0;
        int result = MathInteger.subtraction(numberOne, numberTwo);
        assertEquals(numberOne - numberTwo, result);
    }

    @Test
    void subtractionFirstNumberZero() {
        int numberOne = 0;
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.subtraction(numberOne, numberTwo);
        assertEquals(numberOne - numberTwo, result);
    }

    @Test
    void subtractionSecondNumberZero() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 0;
        int result = MathInteger.subtraction(numberOne, numberTwo);
        assertEquals(numberOne - numberTwo, result);
    }

    @Test()
     void subtractionOverflow() {
        int numberOne = -2147483647;
        int numberTwo = 5;
        int result = MathInteger.subtraction(numberOne, numberTwo);
        assertEquals(numberOne - numberTwo, result);
    }
    @Test
    void divisionPositive() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.division(numberOne, numberTwo);
        assertEquals(numberOne / numberTwo, result);ц   п
    }

    @Test
    void divisionNegative() {
        int numberOne = -1 + (int) (Math.random() * -100);
        int numberTwo = -1 + (int) (Math.random() * -100);
        int result = MathInteger.division(numberOne, numberTwo);
        assertEquals(numberOne / numberTwo, result);
    }

    @Test
    void divisionFirstNumberZero() {
        int numberOne = 0;
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.division(numberOne, numberTwo);
        assertEquals(numberOne / numberTwo, result);
    }

    @Test()
    public void divisionSecondNumberZero() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 0;
        int result = MathInteger.division(numberOne, numberTwo);
        assertEquals(numberOne / numberTwo, result);
    }

    @Test
    void multiplyPositive() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.multiply(numberOne, numberTwo);
        assertEquals(numberOne * numberTwo, result);
    }

    @Test
    void multiplyNegative() {
        int numberOne = -1 + (int) (Math.random() * -100);
        int numberTwo = -1 + (int) (Math.random() * -100);
        int result = MathInteger.multiply(numberOne, numberTwo);
        assertEquals(numberOne * numberTwo, result);
    }

    @Test
    void multiplyZero() {
        int numberOne = 0;
        int numberTwo = 0;
        int result = MathInteger.multiply(numberOne, numberTwo);
        assertEquals(numberOne * numberTwo, result);
    }

    @Test
    void multiplyFirstNumberZero() {
        int numberOne = 0;
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.multiply(numberOne, numberTwo);
        assertEquals(numberOne * numberTwo, result);
    }

    @Test
    void multiplySecondNumberZero() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 0;
        int result = MathInteger.multiply(numberOne, numberTwo);
        assertEquals(numberOne * numberTwo, result);
    }

    @Test()
    void multiplyOverflow() {
        int numberOne = 2147483647;
        int numberTwo = 2;
        int result = MathInteger.multiply(numberOne, numberTwo);
        assertEquals(numberOne * numberTwo, result);
    }

    @Test
    void powPositive() {
        int numberOne = 1 + (int) (Math.random() * 10);
        int numberTwo = 1 + (int) (Math.random() * 10);
        int result = MathInteger.pow(numberOne, numberTwo);
        assertEquals((int) Math.pow(numberOne, numberTwo), result);
    }

    @Test()
    void powNegative() {
        int numberOne = 1 + (int) (Math.random() * 10);
        int numberTwo = -1 + (int) (Math.random() * -10);
        int result = MathInteger.pow(numberOne, numberTwo);
        assertEquals((int) Math.pow(numberOne, numberTwo), result);
    }

    @Test
    void powFirstNumberZero() {
        int numberOne = 0;
        int numberTwo = 1 + (int) (Math.random() * 10);
        int result = MathInteger.pow(numberOne, numberTwo);
        assertEquals(0, result);
    }

    @Test
    void powZero() {
        int numberOne = 0;
        int numberTwo = 0;
        int result = MathInteger.pow(numberOne, numberTwo);
        assertEquals(1, result);
    }

    @Test
    void powSecondNumberZero() {
        int numberOne = 1 + (int) (Math.random() * 10);
        int numberTwo = 0;
        int result = MathInteger.pow(numberOne, numberTwo);
        assertEquals(1, result);
    }

    @Test()
    void powOverFlow() {
        int numberOne = 50 + (int) (Math.random() * 100);
        int numberTwo = 50 + (int) (Math.random() * 100);
        int result = MathInteger.pow(numberOne, numberTwo);
        assertEquals((int) Math.pow(numberOne, numberTwo), result);
    }

    @Test
    void residuePositive() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.residue(numberOne, numberTwo);
        assertEquals(numberOne % numberTwo, result);
    }

    @Test
    void residueNegative() {
        int numberOne = -1 + (int) (Math.random() * -100);
        int numberTwo = -1 + (int) (Math.random() * -100);
        int result = MathInteger.residue(numberOne, numberTwo);
        assertEquals(numberOne % numberTwo, result);
    }

    @Test
    void residueFirstNumberZero() {
        int numberOne = 0;
        int numberTwo = 1 + (int) (Math.random() * 100);
        int result = MathInteger.residue(numberOne, numberTwo);
        assertEquals(0, result);
    }

    @Test()
    void residueSecondNumberZero() {
        int numberOne = 1 + (int) (Math.random() * 100);
        int numberTwo = 0;
        int result = MathInteger.residue(numberOne, numberTwo);
        assertEquals(numberOne % numberTwo, result);
    }
}
*/
